package me.github.fwfurtado.micro9313.rating.api.creation;

import me.github.fwfurtado.micro9313.rating.api.RatingController;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import static org.springframework.http.ResponseEntity.created;

@RatingController
class CreateRatingController {

    private final CreateRatingService service;

    CreateRatingController(CreateRatingService service) {
        this.service = service;
    }


    @PostMapping
    ResponseEntity<?> createRating(@RequestBody @Validated CreateRatingForm form, ServletUriComponentsBuilder uriBuilder) {
        var id = service.createRatingBy(form);

        var uri = uriBuilder.path("/ratings/{id}").build(id);

        return created(uri).build();
    }

    record CreateRatingForm(Long courseId, Long userId, Double value, String comment) {
    }

}
