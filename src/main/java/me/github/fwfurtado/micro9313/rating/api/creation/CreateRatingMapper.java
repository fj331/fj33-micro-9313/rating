package me.github.fwfurtado.micro9313.rating.api.creation;

import me.github.fwfurtado.micro9313.rating.api.creation.CreateRatingController.CreateRatingForm;
import me.github.fwfurtado.micro9313.rating.infra.Mapper;
import me.github.fwfurtado.micro9313.rating.shared.Rating;
import org.springframework.stereotype.Component;

@Component
class CreateRatingMapper implements Mapper<CreateRatingForm, Rating> {
    @Override
    public Rating map(CreateRatingForm source) {
        return new Rating(source.comment(), source.value(), source.courseId(), source.userId());
    }
}
