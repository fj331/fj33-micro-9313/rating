package me.github.fwfurtado.micro9313.rating.api.creation;

import me.github.fwfurtado.micro9313.rating.shared.Rating;

import java.util.Optional;

public interface CreateRatingRepository {
    void save(Rating rating);
    Optional<Rating> findByUserIdAndCourseId(Long userId, Long courseId);
}
