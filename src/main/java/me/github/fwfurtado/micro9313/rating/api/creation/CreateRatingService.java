package me.github.fwfurtado.micro9313.rating.api.creation;

import me.github.fwfurtado.micro9313.rating.api.creation.CreateRatingController.CreateRatingForm;
import me.github.fwfurtado.micro9313.rating.shared.events.CreatedRatingEvent;
import me.github.fwfurtado.micro9313.rating.shared.Rating;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
class CreateRatingService {

    private final CreateRatingMapper mapper;
    private final CreateRatingRepository repository;
    private final ApplicationEventPublisher publisher;

    CreateRatingService(CreateRatingMapper mapper, CreateRatingRepository repository, ApplicationEventPublisher publisher) {
        this.mapper = mapper;
        this.repository = repository;
        this.publisher = publisher;
    }

    public UUID createRatingBy(CreateRatingForm form) {

        repository
            .findByUserIdAndCourseId(form.userId(), form.courseId())
            .ifPresent(this::throwUserAlreadyRating);

        var rating = mapper.map(form);

        repository.save(rating);

        publisher.publishEvent(CreatedRatingEvent.from(rating));

        return rating.getId();
    }

    private void throwUserAlreadyRating(Rating rating) {
        throw new IllegalArgumentException("User already rating this course");
    }

}
