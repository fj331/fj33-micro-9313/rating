package me.github.fwfurtado.micro9313.rating.configurations;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCircuitBreaker
class CircuitBreakerConfiguration {

}
