package me.github.fwfurtado.micro9313.rating.configurations;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
class EventEnricher {
    public <T> Message<T> enrich(T event) {
        return MessageBuilder.withPayload(event).build();
    }
}
