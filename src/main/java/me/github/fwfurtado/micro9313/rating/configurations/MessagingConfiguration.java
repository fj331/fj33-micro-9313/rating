package me.github.fwfurtado.micro9313.rating.configurations;

import me.github.fwfurtado.micro9313.rating.shared.events.CreatedRatingEvent;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableBinding(Source.class)
class MessagingConfiguration {

    private final MessageChannel ratingChannel;
    private final EventEnricher enricher;

    MessagingConfiguration(@Output(Source.OUTPUT) MessageChannel ratingChannel, EventEnricher enricher) {
        this.ratingChannel = ratingChannel;
        this.enricher = enricher;
    }

    @EventListener
    public void handle(CreatedRatingEvent event) {
        var message = enricher.enrich(event);

        ratingChannel.send(message);
    }
}
