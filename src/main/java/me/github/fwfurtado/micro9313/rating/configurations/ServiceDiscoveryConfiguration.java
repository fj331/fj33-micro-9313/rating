package me.github.fwfurtado.micro9313.rating.configurations;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
class ServiceDiscoveryConfiguration {

}
