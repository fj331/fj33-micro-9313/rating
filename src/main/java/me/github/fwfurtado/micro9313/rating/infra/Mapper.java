package me.github.fwfurtado.micro9313.rating.infra;

public interface Mapper<S, T> {

    T map(S source);
}
