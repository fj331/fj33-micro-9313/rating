package me.github.fwfurtado.micro9313.rating.infra;

import me.github.fwfurtado.micro9313.rating.api.creation.CreateRatingRepository;
import me.github.fwfurtado.micro9313.rating.shared.Rating;
import org.springframework.data.repository.Repository;

import java.util.UUID;

public interface RatingRepository extends Repository<Rating, UUID>, CreateRatingRepository {
}
