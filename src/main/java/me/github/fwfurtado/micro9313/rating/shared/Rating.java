package me.github.fwfurtado.micro9313.rating.shared;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String comment;
    private Double value;
    private LocalDateTime date;
    private Long userId;
    private Long courseId;

    protected Rating() {
    }

    public Rating(String comment, Double value, Long courseId, Long userId) {
        this.comment = comment;
        this.value = value;
        this.courseId = courseId;
        this.userId = userId;
        this.date = LocalDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public String getComment() {
        return comment;
    }

    public Double getValue() {
        return value;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Long getCourseId() {
        return courseId;
    }

    public Long getUserId() {
        return userId;
    }
}
