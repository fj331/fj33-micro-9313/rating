package me.github.fwfurtado.micro9313.rating.shared.events;

import me.github.fwfurtado.micro9313.rating.shared.Rating;

import java.time.LocalDateTime;
import java.util.UUID;

public record CreatedRatingEvent(UUID ratingId, LocalDateTime date, Double value, String comment) {
    public static CreatedRatingEvent from(Rating rating) {
        return new CreatedRatingEvent(rating.getId(), rating.getDate(), rating.getValue(), rating.getComment());
    }
}
